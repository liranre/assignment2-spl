//
//  AddCarEvent.cpp
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#include "../include/AddCarEvent.h"



AddCarEvent::AddCarEvent(const AddCarEvent& other):Event(other),simulatorCars(other.simulatorCars) , type(other.type), roadPlan(other.roadPlan){
//    this->copy(other);
}

void AddCarEvent::copy(const AddCarEvent& other){
    Event::copy(other);
    simulatorCars = other.simulatorCars;
    type = other.type;
    roadPlan = other.roadPlan;
}
AddCarEvent& AddCarEvent::operator=(const AddCarEvent& other){
    if (this == &other) {
        return *this;
    }
    this->copy(other);
    return *this;
}


void AddCarEvent::performEvent(){
    Car* toAdd = new Car(carID, roadPlan, eventTime, "");
    simulatorCars->push_back(toAdd);
}

AddCarEvent::~AddCarEvent(){}