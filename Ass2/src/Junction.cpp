//
//  Junction.cpp
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#include "../include/Junction.h"

Junction::Junction() : ID(""),passingRoads(){}

Junction::Junction(string _ID): ID(_ID) , passingRoads(){}

Junction::Junction(string _ID,queue<string> _passingRoads) :ID(_ID) , passingRoads(passingRoads){}
Junction::Junction(const Junction& other): ID(other.ID) , passingRoads(other.passingRoads){}
string Junction::getID() const {
    return ID;
}
queue<string> Junction::getPassingRoads() const{
    return passingRoads;
}
int Junction::getNumOfPassingRoads() const{
    return boost::lexical_cast<int>(passingRoads.size());
}
string Junction::getCurrentGreenRoad(){
    return passingRoads.front();
}

void Junction::addRoadToIncomingRoads(string toAdd){
    passingRoads.push(toAdd);
}

void Junction::dequeuePassingRoads(){
    string toKeep = passingRoads.front();
    passingRoads.pop();
    passingRoads.push(toKeep);
    
}

string Junction::getCurrentGreenRoadStartJunction(){
    string currentRoad = getCurrentGreenRoad();
    std::string roadName;
    for (size_t i = 0; i<currentRoad.length(); i++) {
        if (currentRoad[i] == ','){
            roadName = currentRoad.substr(0,i);
        }
    }
    return roadName;
}
string Junction::getCurrentGreenRoadEndJunction(){
    string currentRoad = getCurrentGreenRoad();
    std::string roadName;
    for (size_t i = 0; i<currentRoad.length(); i++) {
        if (currentRoad[i] == ','){
            roadName = currentRoad.substr(i+1);
        }
    }
    return roadName;

}

Junction& Junction::operator=(const Junction& other){
    ID = other.ID;
    passingRoads = other.passingRoads;
    return *this;
}

Junction::~Junction(){}