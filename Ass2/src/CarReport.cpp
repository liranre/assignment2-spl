//
//  CarReport.cpp
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#include "../include/CarReport.h"

CarReport::CarReport(string _carReportID , int _time,string _carID, Car* _car) : Report(_carReportID,_time) ,carID(_carID), car(_car) {}

void CarReport::writeReport(){
    boost::property_tree::ptree tree;
    boost::property_tree::ini_parser::read_ini("Report.ini", tree);
    string initial = "report_" + reportID;
    tree.add(initial + ".carId", car->getID());
    tree.add(initial + ".history", car->getHistory());
    tree.add (initial + ".faultyTimeLeft", boost::lexical_cast<string>(car->getFaultTime()));
    boost::property_tree::ini_parser::write_ini("Report.ini", tree);
}


CarReport::CarReport(const CarReport& other): Report(other), carID(other.carID) , car(other.car){
    //copy(other);
}
void CarReport::copy(const CarReport& other){
    Report::copy(other);
    car = other.car;
}
CarReport& CarReport::operator=(const CarReport& other){
    if (this == &other) {
        return *this;
    }
    this->copy(other);
    return *this;
}

string CarReport::getCarID(){
    return carID;
}
string CarReport::getType(){
    return "car_report";
}
Car* CarReport::getCar(){
    return car;
}
CarReport::~CarReport(){}



