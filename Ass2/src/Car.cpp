//
//  Car.cpp
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#include "../include/Car.h"


Car::Car():ID(""),roadPlan(vector<Road*>()),faultTime(0),location(0),timeEntered(0),history(""),currentPlace(0),condition(1){}
Car::Car(string _ID,vector<Road*> _roadPlan,int _timeEntered,string _history):ID(_ID),roadPlan(_roadPlan),faultTime(0),location(0),timeEntered(_timeEntered),history(_history),currentPlace(0),condition(1){}
Car::Car(string _ID,vector<Road*> _roadPlan,int _timeEntered,int _condition,int _faultTime , string _history, double _location,int _currentPlace):ID(_ID),roadPlan(_roadPlan),faultTime(_faultTime),location(_location),timeEntered(_timeEntered),history(_history),currentPlace(_currentPlace),condition(_condition){}
Car::Car(const Car& other) : ID(other.ID) , roadPlan(other.roadPlan) , faultTime(other.faultTime) , location(other.location) , timeEntered(other.timeEntered), history(other.history) , currentPlace(other.currentPlace) , condition(other.condition){
//    ID=other.ID;
//    roadPlan = other.roadPlan;
//    faultTime = other.faultTime;
//    location = other.location;
//    timeEntered = other.timeEntered;
//    history = other.history;
//    currentPlace = other.currentPlace;
//    condition = other.condition;

}
string Car::getID() const{
    return ID;
}
vector<Road*> Car::getRoadPlan() const {
    return roadPlan;
}
int Car:: getTimeArrival() const{
    return timeEntered;
}
int Car::getCondition() const{
    return condition;
}
int Car::getFaultTime() const{
    return faultTime;
}
string Car::getHistory() const{
    return history;
}
double Car::getLocation() const{
    return location;
}
int Car::getPlace() const{
    return currentPlace;
}
Road* Car::getCurrentRoad() const{
    size_t place = boost::lexical_cast<size_t>(getPlace());
    if (place < roadPlan.size()) {
        return roadPlan.at(place);
    }
    else return 0;
}
void Car::advanceInRoadPlan(){
    currentPlace++;
}
Car& Car::operator=(const Car& other){
    ID=other.ID;
    roadPlan = other.roadPlan;
    faultTime = other.faultTime;
    location = other.location;
    timeEntered = other.timeEntered;
    history = other.history;
    currentPlace = other.currentPlace;
    condition = other.condition;
    return *this;
}

void Car::setLocation(double _newLocation){
    location=_newLocation;
}
void Car::setTimeArrival(int _newTimeEntered){
    timeEntered=_newTimeEntered;
}
void Car::setCondition(int _newCondition){
    condition=_newCondition;
}
void Car::setFaultTime(int _newFaultTime){
    faultTime=_newFaultTime;
    if (faultTime==0) {
        condition=1;
    }
}
void Car::updateHistory(string newHistory){
    history= history+newHistory;
}
bool Car::compare(const Car* car1,const Car* car2){
    return (car1->getLocation()>car2->getLocation());
}
Car::~Car(){}
