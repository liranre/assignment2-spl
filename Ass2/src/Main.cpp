//
//  Main.cpp
//  Ass2
//
//  Created by Liran Revivo on 11/30/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#include "../include/Main.h"

int main (int argc, char *argv[]){
    Simulator *simulator = new Simulator();
    simulator->beginSimulation();
    delete simulator;
}