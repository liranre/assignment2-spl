//
//  Road.cpp
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#include "../include/Road.h"

Road::Road(): ID(""),length(0),startJunction(), endJunction() , numOfFaultyCars(0),currentTimeSlice(0),timeLeftFromTimeSlice(0),numOfPassingCars(0),hasGreenLight(0),carsOnRoad(list<Car*>()),passingCars(list<Car*>()),startJunctionID(""),endJunctionID(""){}
Road::Road(double _length , Junction* _startJunction , Junction* _endJunction , int _timeSlice,int _hasGreentLight , int _numOfFaultyCars) : ID(_startJunction->getID()+"," + _endJunction->getID()),length(_length),startJunction(_startJunction),endJunction(_endJunction),numOfFaultyCars(_numOfFaultyCars),currentTimeSlice(_timeSlice),timeLeftFromTimeSlice(_timeSlice),numOfPassingCars(0),hasGreenLight(_hasGreentLight),carsOnRoad(list<Car*>()), passingCars(list<Car*>()),startJunctionID(_startJunction->getID()), endJunctionID(_endJunction->getID()){}
Road::Road(double _length , Junction* _startJunction , Junction* _endJunction , int _numOfFaultyCars, int _timeSlice , int _timeLeftFromTimeSlice , int _numOfPassingCars ,int _hasGreentLight , list<Car*> _carsOnRoad , list<Car*> _passingCars) : ID(_startJunction->getID() + "," +_endJunction->getID() ) , length(_length), startJunction(_startJunction) , endJunction(_endJunction), numOfFaultyCars(_numOfFaultyCars),currentTimeSlice(_timeSlice) , timeLeftFromTimeSlice(_timeLeftFromTimeSlice), numOfPassingCars(_numOfPassingCars), hasGreenLight(_hasGreentLight) , carsOnRoad(_carsOnRoad) , passingCars(_passingCars),startJunctionID(_startJunction->getID()),endJunctionID(_endJunction->getID()){}
Road::Road(const Road& other):ID(other.ID) , length(other.length) , startJunction(other.startJunction) , endJunction(other.endJunction) , numOfFaultyCars(other.numOfFaultyCars), currentTimeSlice(other.currentTimeSlice), timeLeftFromTimeSlice(other.timeLeftFromTimeSlice) , numOfPassingCars(other.numOfPassingCars), hasGreenLight(other.hasGreenLight) , carsOnRoad(other.carsOnRoad) , passingCars(other.passingCars) , startJunctionID(other.startJunctionID), endJunctionID(other.endJunctionID){
//    ID=other.ID;
//    length = other.length;
//    startJunction = other.startJunction;
//    endJunction = other.endJunction;
//    numOfFaultyCars= other.numOfFaultyCars;
//    currentTimeSlice = other.currentTimeSlice;
//    timeLeftFromTimeSlice = other.timeLeftFromTimeSlice;
//    numOfPassingCars = other.numOfPassingCars;
//    hasGreenLight = other.hasGreenLight;
//    carsOnRoad = other.carsOnRoad;
//    passingCars = other.passingCars;
//    startJunctionID = other.startJunctionID;
//    endJunctionID = other.endJunctionID;
}

string Road::getStartJunctionID() const{
    return startJunctionID;
}
string Road::getEndJunctionID() const{
    return endJunctionID;
}

string Road::getID() const{
    return ID;
}
double Road::getLength() const{
    return length;
}
Junction* Road::getStartJunction() const{
    return startJunction;
}
Junction* Road::getEndJunction() const{
    return endJunction;
}
int Road::getNumOfCars() const{
    
    return boost::lexical_cast<int>(carsOnRoad.size());
}
int Road::getNumOfPassingCars() const{
    return boost::lexical_cast<int>(passingCars.size());
}
int Road::getTimeSlice() const{
    return currentTimeSlice;
}
int Road::getTimeLeftFromTimeSlice() const{
    return timeLeftFromTimeSlice;
}
list<Car*> Road::getCarsOnRoad() const{
    return carsOnRoad;
}
list<Car*> Road::getPassingCars() const{
    return passingCars;
}
int Road::getHasGreentLight() const{
    return hasGreenLight;
}
int Road::getNumOfFaultyCars() const{
    return numOfFaultyCars;
}

bool Road::isGreen() const{
    return (hasGreenLight==1) ? true : false;
}

void Road::setLight(int light){
    hasGreenLight= light;
}

void Road::setTimeLeftFromTimeSlice(int leftTimeSlice){
    timeLeftFromTimeSlice=leftTimeSlice;
}
void Road::setNumPassingCars(int numOfCarsEntered){
    numOfPassingCars = numOfCarsEntered;
}
void Road::setTimeSlice(int timeSlice){
    currentTimeSlice = timeSlice;
}
void Road::addCarToRoad(Car* toAdd){
    carsOnRoad.push_back(toAdd);
}

void Road::addCarToPassingRoad(Car* toAdd){
    passingCars.push_back(toAdd);
}

void Road::removeFromRoad(Car* toRemove){
    carsOnRoad.remove(toRemove);
}

void Road::removeFromPassing(Car* toRemove){
    passingCars.remove(toRemove);
}

void Road::sortCarsOnRoad(){
    carsOnRoad.sort(Car::compare);
}
string Road::printCarsOnRoad() {
    if (carsOnRoad.size() == 0) return "";
    
    string result = "";
    for ( list<Car*>::iterator it = carsOnRoad.begin(); it != carsOnRoad.end() ; it++) {
        result = result + (*it)->getID() +",";
    }
    result = result.substr(0, result.size()-1); // removes the last ","
    return result;
}

Road& Road::operator=(const Road& other){
    if (this==&other) {
        return *this;
    }
    else{
        ID=other.ID;
        length = other.length;
        startJunction = other.startJunction;
        endJunction = other.endJunction;
        numOfFaultyCars= other.numOfFaultyCars;
        currentTimeSlice = other.currentTimeSlice;
        timeLeftFromTimeSlice = other.timeLeftFromTimeSlice;
        numOfPassingCars = other.numOfPassingCars;
        hasGreenLight = other.hasGreenLight;
        carsOnRoad = other.carsOnRoad;
        passingCars = other.passingCars;
        startJunctionID = other.startJunctionID;
        endJunctionID = other.endJunctionID;

        return *this;
    }
}
Road::~Road(){} // nothing to destruct


