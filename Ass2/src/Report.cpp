//
//  Report.cpp
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#include "../include/Report.h"

Report::Report(string _reportID, int _time): reportID(_reportID) , time(_time){}

Report::Report(const Report& other):reportID(other.reportID), time(other.time){
    //copy(other);
}
void Report::copy(const Report& other){
    reportID=other.reportID;
    time = other.time;
    
}

Report& Report::operator=(const Report& other){
    if (this == &other) {
        return *this;
    }
    this->copy(other);
    return *this;
}

int Report::getTime(){
    return time;
}

string Report::getReportID(){
    return reportID;
}

bool Report::operator<(const Report& otherReport){
    return time <= otherReport.time;
}

Report::~Report(){}