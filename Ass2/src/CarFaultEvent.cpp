//
//  CarFaultEvent.cpp
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#include "../include/CarFaultEvent.h"

CarFaultEvent::CarFaultEvent(): Event(), faultTime(0),car(),type("car_fault"){}



CarFaultEvent::CarFaultEvent(const CarFaultEvent& other):Event(other) , faultTime(other.faultTime) , car(other.car) , type(other.type){
    //copy(other);
}

void CarFaultEvent::copy(const CarFaultEvent& other){
    Event::copy(other);
    type = other.type;
    faultTime = other.faultTime;
}

int CarFaultEvent::getFaultTime(){
    return faultTime;
}

CarFaultEvent& CarFaultEvent::operator=(const CarFaultEvent& other){
    if (this == &other) {
        return *this;
    }
    this->copy(other);
    return *this;
}

void CarFaultEvent::performEvent(){
    car->setFaultTime(faultTime);
}

CarFaultEvent::~CarFaultEvent(){}