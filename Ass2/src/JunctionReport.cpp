//
//  JunctionReport.cpp
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#include "../include/JunctionReport.h"

JunctionReport::JunctionReport(string _reportID, int _time, Junction *_junction,list<Road*> _incomingRoads): Report(_reportID,_time), junction(_junction) , comingRoads(_incomingRoads){}

void JunctionReport::writeReport(){
    boost::property_tree::ptree tree;
    boost::property_tree::ini_parser::read_ini("Report.ini", tree);
    string initial = "report_" + reportID;
    tree.add(initial + ".junctionId", junction->getID());
    string timeSlices="";
    for(list<Road*>::iterator iterRoad=comingRoads.begin(); iterRoad!=comingRoads.end(); iterRoad++){
        int timeLeft=-1;
        if((*iterRoad)->getHasGreentLight()==1)
            timeLeft=(*iterRoad)->getTimeLeftFromTimeSlice();
        timeSlices=timeSlices+"("+boost::lexical_cast<string>((*iterRoad)->getTimeSlice())+","+boost::lexical_cast<string>(timeLeft)+")";
    }
    tree.add(initial + ".timeSlices", timeSlices);
    for(list<Road*>::iterator iterRoad=comingRoads.begin(); iterRoad!=comingRoads.end(); ++iterRoad){
        string cars="";
        list<Car*> carsOnRoad= (*iterRoad)->getCarsOnRoad();
        for(list<Car*>::iterator iterCars=carsOnRoad.begin(); iterCars!=carsOnRoad.end(); ++iterCars){
            cars=cars+"("+ boost::lexical_cast<string>((*iterCars)->getID())+")";
        }
        tree.add(initial+"."+boost::lexical_cast<string>((*iterRoad)->getStartJunction()->getID()),cars);
    }
    boost::property_tree::ini_parser::write_ini("Report.ini", tree);
}

JunctionReport::JunctionReport(const JunctionReport& other): Report(other) , junction(other.junction), comingRoads(other.comingRoads){
    //copy(other);
}
void JunctionReport::copy(const JunctionReport& other){
    Report::copy(other);
    junction = other.junction;
}
JunctionReport& JunctionReport::operator=(const JunctionReport& other){
    if (this == &other) {
        return *this;
    }
    this->copy(other);
    return *this;
}

string JunctionReport::getType(){
    return "junction_report";
}
JunctionReport::~JunctionReport(){}

