//
//  RoadReport.cpp
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#include "../include/RoadReport.h"

RoadReport::RoadReport(string _id, int _time, Road* _road): Report(_id, _time), road(_road) {}

void RoadReport::writeReport() {
    // cout << "ROAD REPORT" << endl;
    boost::property_tree::ptree tree;
    read_ini("Report.ini", tree);
    
    string id = boost::lexical_cast<string>(reportID);
    string section = "report_" + id;

    tree.add(section + ".startJunction", road->getStartJunctionID());
    tree.add(section + ".endJunction", road->getEndJunctionID());
    tree.add(section + ".cars",  road->printCarsOnRoad());
    
    write_ini( "Report.ini", tree );
}

RoadReport::RoadReport(const RoadReport& other): Report(other),road(other.road) {
   // copy(other);
}
void RoadReport::copy(const RoadReport& other){
    Report::copy(other);
    road = other.road;
}
RoadReport& RoadReport::operator=(const RoadReport& other){
    if (this == &other) {
        return *this;
    }
    this->copy(other);
    return *this;
}

string RoadReport::getType(){
    return "road_report";
}
RoadReport::~RoadReport(){}
