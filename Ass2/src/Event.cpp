
//
//  Event.cpp
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#include "../include/Event.h"

Event::Event(): eventID("") , eventTime(0) , carID(""){}

Event::Event(string _eventID, int _eventTime, string _carID): eventID(_eventID), eventTime(_eventTime) , carID(_carID){
//    eventID = _eventID;
//    eventTime = _eventTime;
//    carID = _carID;
}

Event::Event(const Event& other):eventID(other.eventID) , eventTime(other.eventTime) , carID(other.carID){
//    copy(other);
}

void Event::copy(const Event& other){
    eventID = other.eventID;
    eventTime = other.eventTime;
    carID = other.carID;
}

string Event::getID(){
    return eventID;
}

int Event::getTime(){
    return eventTime;
}

string Event::getCarID(){
    return carID;
}


Event& Event::operator=(const Event& other){
    if (this == &other) {
        return *this;
    }
    this->copy(other);
    return *this;
}

Event::~Event(){}


