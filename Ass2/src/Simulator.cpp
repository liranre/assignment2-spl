//
//  Simulator.cpp
//  Ass2
//
//  Created by Liran Revivo on 11/25/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#include "../include/Simulator.h"


Simulator::Simulator() : cars(vector<Car*>()), roads(vector<Road*>()) , junctions(vector<Junction*>()) , reports(vector<Report*>()), events(vector<Event*>()), MAX_SPEED(0) , TERMINATION_TIME(NO_TERMINATION) , DEFAULT_TIME_SLICE(0), MIN_TIME_SLICE(0), MAX_TIME_SLICE(0){
    readEvents();
    readCommands();
    readConfiguration();
    readRoadMap();
    beginSimulation();
}

void Simulator::beginSimulation(){
    for (int i = 1; i<TERMINATION_TIME; i++) {
        this->performSimulationForTime(i);
    }
}

void Simulator::performSimulationForTime(int time){
    this->performEvents(time);
    this->updateCarHistory(time);
    this->performCommands(time);
    this->advanceCars(time);
    this->updateCars(time);
    this->updateGreenLights(time);
}

void Simulator::performEvents(int time){
    for (vector<Event*>::iterator iter = events.begin(); iter != events.end(); iter++) {
        if ((*iter)->getTime() == time) {
            (*iter)->performEvent();
        }
    }
}

void Simulator::updateCarHistory(int time){
    for (vector<Car*>::iterator iter = cars.begin(); iter != cars.end(); iter++) {
        if (boost::lexical_cast<size_t>((*iter)->getPlace()) < (*iter)->getRoadPlan().size()) {
            string Stime = boost::lexical_cast<string>(time);
            string id = (*iter)->getRoadPlan()[(boost::lexical_cast<size_t>((*iter)->getPlace()))]->getID();
            string location = boost::lexical_cast<string>((*iter)->getLocation());
            string history = "(" + Stime + "," + id + "," + location + ")";
            (*iter)->updateHistory(history);
        }
    }
}

void Simulator::advanceCars(int time){
    advanceCarsOnRoad(time);
    advanceCarsOnJunction(time);
}

void Simulator::advanceCarsOnRoad(int time){
    vector<Road*> relevantRoads = vector<Road*>();
    for (vector<Car*>::iterator iter = cars.begin(); iter != cars.end(); iter++) {
        Road *curRoad = (*iter)->getCurrentRoad();
        if (curRoad != 0) {
            relevantRoads.push_back(curRoad);
        }
    }
    for (vector<Road*>::iterator iter = relevantRoads.begin(); iter != relevantRoads.end(); iter++) {
        (*iter)->sortCarsOnRoad();
        list <Car*> carsOnRoad = (*iter)->getCarsOnRoad();
        int numOfFaultyCarsBefore = 0;
        double locationOfTheLastFaultyCar = 0;
        int numOfFaultyCarsOnTheSameLocation = 0;
        list<Car*> carsToRemove = list<Car*>();
        for (list<Car*>::iterator it = carsOnRoad.begin(); it != carsOnRoad.end(); it++) {
            if ((*it)->getCondition() == 0) {
                numOfFaultyCarsBefore++;
                if (locationOfTheLastFaultyCar == (*it)->getLocation()) {
                    numOfFaultyCarsOnTheSameLocation++;
                }
                else{
                    locationOfTheLastFaultyCar = (*it)->getLocation();
                    numOfFaultyCarsOnTheSameLocation = 1;
                }
            }
            else{
                int faultyCars;
                int normalSpeed = ceil((*iter)->getLength()/(*iter)->getNumOfCars());
                if (locationOfTheLastFaultyCar == (*it)->getLocation()) {
                    faultyCars = numOfFaultyCarsBefore - numOfFaultyCarsOnTheSameLocation;
                }
                else{
                    faultyCars = numOfFaultyCarsBefore;
                    numOfFaultyCarsOnTheSameLocation = 1;
                }
                int newSpeed = ceil(boost::lexical_cast<double>(normalSpeed)/pow(2, faultyCars));
                newSpeed = fmin(newSpeed, MAX_SPEED);
                double prevLocation = (*it)->getLocation();
                double newLocation = prevLocation + newSpeed;
                (*it)->setLocation(fmin(newLocation, (*iter)->getLength()));
                if ((*it)->getLocation() == (*iter)->getLength() && prevLocation != (*iter)->getLength()) {
                    carsToRemove.push_back(*it);
                }
            }
        }
        for (list<Car*>::iterator itera = carsToRemove.begin(); itera != carsToRemove.end(); itera++) {
            if (boost::lexical_cast<size_t>((*itera)->getPlace()) < (*itera)->getRoadPlan().size()) {
                Road *curRoad = ((*itera)->getRoadPlan().at(boost::lexical_cast<size_t>((*itera)->getPlace())));
                curRoad->addCarToPassingRoad(*itera);
            }
        }
        (*iter)->sortCarsOnRoad();
    }
}

void Simulator::performCommands(int time){
    for (vector<Report*>::iterator iter = reports.begin(); iter != reports.end(); iter++) {
        if ((*iter)->getTime() == time) {
            (*iter)->writeReport();
        }
    }
}

void Simulator::advanceCarsOnJunction(int time){
    vector<Road*> roadsWithGreenLight = vector<Road*>();
    for (vector<Junction*>::iterator iter = junctions.begin(); iter != junctions.end(); iter++) {
        string greenRoadStartJunctionID = (*iter)->getCurrentGreenRoadStartJunction();
        string greenRoadEndJunctionID = (*iter)->getCurrentGreenRoadEndJunction();
        Road *road = findRoad(greenRoadStartJunctionID, greenRoadEndJunctionID);
        roadsWithGreenLight.push_back(road);
    }
    
    for (vector<Road*>::iterator iter = roadsWithGreenLight.begin(); iter != roadsWithGreenLight.end(); iter++) {
        if ((*iter)->isGreen() && (*iter)->getNumOfPassingCars()!=0) {
            list<Car*> passingCars = (*iter)->getPassingCars();
            list<Car*>::iterator iterCar = passingCars.begin();
            while ((iterCar != passingCars.end()) && ((*iterCar)->getCondition() != 1)) {
                iterCar++;
            }
            if (iterCar != (*iter)->getPassingCars().end()) {
                (*iter)->setNumPassingCars((*iter)->getNumOfPassingCars()+1);
                Car *carToPass = *iterCar;
                (*iter)->removeFromPassing(carToPass);
                (*iter)->removeFromRoad(carToPass);
                (*iterCar)->advanceInRoadPlan();
                size_t cur = boost::lexical_cast<size_t>(carToPass->getPlace());
                if ((cur < (*iterCar)->getRoadPlan().size()) && (cur != 0)) {
                    Road *nextRoad = carToPass->getRoadPlan().at(cur);
                    nextRoad->addCarToRoad(carToPass); //ATTENTION MIGHT CHANGE THIS//
                    //±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±//
                    carToPass->setLocation(0);
                }
                else{
                    string history = "(" + boost::lexical_cast<string>(time+1) + "," + (*iterCar)->getRoadPlan().at((*iterCar)->getPlace()-1)->getID() + "," + boost::lexical_cast<string>((*iterCar)->getLocation()) + ")";
                    (*iterCar)->updateHistory(history);
                }
            }
        }
    }
}
// update fault time for faulty cars
void Simulator::updateCars(int time){
    for (vector<Car*>::iterator iter = cars.begin(); iter != cars.end(); iter++) {
        Car *car = *iter;
        if (car->getFaultTime() != 0) {
            car->setFaultTime(car->getFaultTime()-1);
        }
    }
}

void Simulator::readCommands() {
    boost::property_tree::ptree tree;
    boost::property_tree::ini_parser::read_ini("Commands.ini", tree);
    
    string id;
    string carId;
    string startJunction;
    string endJunction;
    string junctionId;
    int time;
    Report * report;
    
    for (boost::property_tree::ptree::const_iterator section = tree.begin();section != tree.end(); section++) {
        string type =  tree.get<string>(section->first+".type");
        time = tree.get<int>(section->first+".time");
        
        
        if (type == "termination") {
            if (TERMINATION_TIME == NO_TERMINATION) {
                TERMINATION_TIME = time;
            }
        } else {
            if (type == "car_report") {
                id = tree.get<string>(section->first+".id");
                carId = tree.get<string>(section->first+".carId");
                Car *  toReport = findCar(carId);
                report = new CarReport(id, time,carId,toReport);
            }
            // create car report
            else if (type == "road_report") {
                id = tree.get<string>(section->first+".id");
                startJunction = tree.get<string>(section->first+".startJunction");
                endJunction = tree.get<string>(section->first+".endJunction");
                report = new RoadReport(id, time, findRoad(startJunction, endJunction));
                
            }
            // create Junction report
            else if (type == "junction_report") {
                id = tree.get<string>(section->first+".id");
                junctionId = tree.get<string>(section->first+".junctionId");
                string junction = id;
                list<Road*> incomingRoads;

                for(int i=0; i< findJunction(id)->getNumOfPassingRoads(); i++)
                {
                    string start = findJunction(id)->getCurrentGreenRoadStartJunction();
                    string end = findJunction(id)->getCurrentGreenRoadEndJunction();
                    findJunction(id)->dequeuePassingRoads();
                    Road *road=findRoad(start, end);
                    incomingRoads.push_back(road);
                }
                report = new JunctionReport(id, time, findJunction(junctionId),incomingRoads);
            }
            
            // flag indicated if inserted
            vector<Report*>::iterator it;
            bool flag = false;
            for (it = reports.begin(); it != reports.end() ; it++) {
                if (report->getTime() < (*it)->getTime()) {
                    reports.insert(it, report);
                    flag = true;
                    break;
                }
            }
            if (!flag)
                reports.push_back(report);
        }
    }
}

void Simulator::readEvents() {
    boost::property_tree::ptree tree;
    boost::property_tree::ini_parser::read_ini("Events.ini", tree);
    
    Event *event;
    vector<Road*> roadPlan;
    
    for (boost::property_tree::ptree::const_iterator section = tree.begin();section != tree.end(); section++) {
        string type =  tree.get<string>(section->first+".type");
        int time = tree.get<int>(section->first+".time");
        string id = tree.get<string>(section->first+".carId");
        
        if (type == "car_arrival") {
            string roadPlanStr = tree.get<string>(section->first+".roadPlan");
            stringstream stream(roadPlanStr);
            vector<std::string> stringsVector;
            string singleString;
            
            while(std::getline(stream, singleString, ','))
            {
                stringsVector.push_back(singleString);
            }
            
            vector<string>::iterator it;
            for (it = stringsVector.begin(); it != stringsVector.end()-1 ; it++) {
                string sJ = (*it);
                it++;
                string eJ = (*it);
                Road * r = findRoad(sJ,eJ);
                roadPlan.push_back(r);
                it--;
            }
            event = new AddCarEvent(section->first, time, id, roadPlan, &cars);
            roadPlan.clear();
        }
        else if (type == "car_fault") {
            int timeOfFault = tree.get<int>(section->first+".timeOfFault");
            Car* car = findCar(id);
            event = new CarFaultEvent(section->first, time, id, timeOfFault, car);
        }
        

        vector<Event*>::iterator it;
        bool flag = false; //represents if inserted
        for (it = events.begin(); it != events.end() ; it++) {
            if (event->getTime() < (*it)->getTime()) {
                events.insert(it, event);
                flag = true;
                break;
            }
        }
        
        if (!flag)
            events.push_back(event);
    }
}

Simulator::Simulator(const Simulator& other) : cars(other.cars), roads(other.roads) , junctions(other.junctions) , reports(other.reports) , events(other.events) , MAX_SPEED(other.MAX_SPEED) , TERMINATION_TIME(other.TERMINATION_TIME), DEFAULT_TIME_SLICE(other.DEFAULT_TIME_SLICE) , MIN_TIME_SLICE(other.MIN_TIME_SLICE) , MAX_TIME_SLICE(other.MAX_TIME_SLICE){
//    cars = other.cars;
//    roads = other.roads;
//    junctions = other.junctions;
//    reports = other.reports;
//    events = other.events;
//    MAX_SPEED = other.MAX_SPEED;
//    TERMINATION_TIME = other.TERMINATION_TIME;
//    DEFAULT_TIME_SLICE = other.DEFAULT_TIME_SLICE;
//    MIN_TIME_SLICE = other.MIN_TIME_SLICE;
//    MAX_TIME_SLICE = other.MAX_TIME_SLICE;
}

Simulator& Simulator::operator=(const Simulator& other){
    cars = other.cars;
    roads = other.roads;
    junctions = other.junctions;
    reports = other.reports;
    events = other.events;
    MAX_SPEED = other.MAX_SPEED;
    TERMINATION_TIME = other.TERMINATION_TIME;
    DEFAULT_TIME_SLICE = other.DEFAULT_TIME_SLICE;
    MIN_TIME_SLICE = other.MIN_TIME_SLICE;
    MAX_TIME_SLICE = other.MAX_TIME_SLICE;
    return *this;
}



void Simulator::updateGreenLights(int time){
    // for each junction
    for (vector<Junction*>::iterator iter = junctions.begin(); iter != junctions.end(); iter++) {
        Junction *junctionToUpdate = *iter;
        // if it has passing roads
        if (junctionToUpdate->getNumOfPassingRoads() != 0) {
            //get its current green road
            Road *roadToUpdate = findRoad(junctionToUpdate->getCurrentGreenRoadStartJunction(), junctionToUpdate->getCurrentGreenRoadEndJunction());
            // if it has not done its time slice , decrements it time slice by 1 and continue
            if (roadToUpdate->getTimeLeftFromTimeSlice() != 1) {
                roadToUpdate->setTimeLeftFromTimeSlice(roadToUpdate->getTimeLeftFromTimeSlice()-1);
            }
            // otherwise(it should end its time slice)
            else{
                int timeSlice;
                // if in each time unit in the time slice, a car passed , then do the following formula
                if (roadToUpdate->getNumOfPassingCars() == roadToUpdate->getTimeSlice()) {
                    timeSlice = fmin(roadToUpdate->getTimeSlice()+1, MAX_TIME_SLICE);
                }
                else{
                    // if no car passed during the last time slice , the do the following formula
                    if (roadToUpdate->getNumOfPassingCars() == 0) {
                        timeSlice = fmax(roadToUpdate->getTimeSlice()-1, MIN_TIME_SLICE);
                    }
                    // cars did pass during the time slice ,but not in each time unit in the time slice.
                    else{
                        timeSlice = roadToUpdate->getTimeSlice();
                    }
                }
                // reset
                roadToUpdate->setNumPassingCars(0);
                roadToUpdate->setTimeLeftFromTimeSlice(timeSlice);
                roadToUpdate->setTimeSlice(timeSlice);
                roadToUpdate->setLight(0);
                // the time slice for the current road for green light passed, get the next road on the queue to be the green road
                junctionToUpdate->dequeuePassingRoads();
                Road* road = findRoad(junctionToUpdate->getCurrentGreenRoadStartJunction(), junctionToUpdate->getCurrentGreenRoadEndJunction());
                road->setLight(1);
            }
        }
    }
}

void Simulator::readConfiguration() {
    boost::property_tree::ptree tree;
    boost::property_tree::ini_parser::read_ini("Configuration.ini", tree);
    
    for (boost::property_tree::ptree::const_iterator section = tree.begin();section != tree.end(); section++) {
        MAX_SPEED = tree.get<int>(section->first+".MAX_SPEED");
        DEFAULT_TIME_SLICE = tree.get<int>(section->first+".DEFAULT_TIME_SLICE");
        MAX_TIME_SLICE = tree.get<int>(section->first+".MAX_TIME_SLICE");
        MIN_TIME_SLICE = tree.get<int>(section->first+".MIN_TIME_SLICE");
    }
}

void Simulator::readRoadMap(){
    boost::property_tree::ptree pt;
    boost::property_tree::ini_parser::read_ini("RoadMap.ini", pt);
    for (boost::property_tree::ptree::iterator section = pt.begin();section != pt.end(); section++) {
        string junctionID=section->first;
        ///// is the junction with junctionID already in database?
        bool junctionFlag = true;
        for (vector<Junction*>::iterator juncIterator = junctions.begin(); juncIterator!=junctions.end() && junctionFlag; juncIterator++) {
            if (((*juncIterator)->getID()) == junctionID) {
                junctionFlag = false;
            }
        }
        // if this junction wasnt in the database , add it .
        if(junctionFlag){
            Junction* toAdd= new Junction(junctionID);
            junctions.push_back(toAdd);
        }
        for (boost::property_tree::ptree::iterator property = section->second.begin() ;property != section->second.end(); property++) {
            string otherJunctionID= property->first;
            // is the junction with otherJunctionID already in database?
            bool otherJunctionFlag = true;
            for (vector<Junction*>::iterator juncIterator = junctions.begin(); juncIterator!=junctions.end() && otherJunctionFlag; juncIterator++) {
                if (((*juncIterator)->getID()) == otherJunctionID) {
                    otherJunctionFlag = false;
                }
            }
            // if this junction wasnt in the database , add it.
            if(otherJunctionFlag){
                Junction* otherToAdd= new Junction(otherJunctionID);
                junctions.push_back(otherToAdd);
            }
            double length= boost::lexical_cast<double>(property->second.data());
            string roadID= otherJunctionID+","+junctionID;
            Junction * junc = findJunction(junctionID);
            junc->addRoadToIncomingRoads(roadID);
            // is the road with RoadID already in database?
            bool roadFlag = true;
            for (vector<Road*>::iterator roadIterator = roads.begin(); roadIterator!= roads.end() && roadFlag; roadIterator++) {
                if ((*roadIterator)->getID() == roadID) {
                    roadFlag = false;
                }
            }
            // if this road wasnt in the database , add it.s
            if(roadFlag){
                Junction* stJunc = findJunction(otherJunctionID);
                Junction* enJunc = findJunction(junctionID);
                Road* roadToAdd = new Road(length,stJunc,enJunc,DEFAULT_TIME_SLICE,0,0);
                roads.push_back(roadToAdd);
                
            }
        }
    }
}



int Simulator::getTerminationTime(){
    return TERMINATION_TIME;
}
int Simulator::getMaxSpeed(){
    return MAX_SPEED;
}
int Simulator::getMaxTimeSlice(){
    return MAX_TIME_SLICE;
}
int Simulator::getMinTimeSlice(){
    return MIN_TIME_SLICE;
}
int Simulator::getDefaultTimeSlice(){
    return DEFAULT_TIME_SLICE;
}

Junction* Simulator::findJunction(string junctionID){
    // binary search on vector
    size_t leftMark = 0;
    size_t rightMark = junctions.size()-1;
    size_t middleMark = (leftMark+rightMark)/2;
    Junction *ans;
    
    while (leftMark<=rightMark){
        middleMark = (leftMark+rightMark)/2;
        ans = junctions[middleMark];
        
        if (junctionID == ans->getID()) {
            return ans;
        }
        else if (junctionID > ans->getID()){
            leftMark = middleMark + 1;
        }
        else{
            rightMark = middleMark - 1;
        }
    }
    std::cout<< " Reached NULL findJunction" <<std::endl;
    return NULL;
}

Road* Simulator::findRoad (string startJunction, string endJunction){
    // binary search on vector of roads
    size_t leftMark = 0;
    size_t rightMark = roads.size()-1;
    size_t middleMark = (leftMark+rightMark)/2;
    Road *ans;
    
    while (leftMark<=rightMark){
        middleMark = (leftMark+rightMark)/2;
        ans = roads[middleMark];
        
        if (startJunction == ans->getStartJunctionID() && endJunction == ans->getEndJunctionID()) {
            return ans;
        }
        else if (endJunction > ans->getEndJunctionID()){
            leftMark = middleMark + 1;
        }
        else if (endJunction < ans->getEndJunctionID()){
            rightMark = middleMark - 1;
        }
        else if (startJunction > ans->getStartJunctionID()){
            leftMark = middleMark + 1;
        }
        else if (startJunction < ans->getStartJunctionID()){
            rightMark = middleMark - 1;
        }
    }
    std::cout<< " Reached NULL findRoad" <<std::endl;
    return NULL;
}

Car* Simulator::findCar (string carID){
    // binary search on vector
    size_t leftMark = 0;
    size_t rightMark = cars.size()-1;
    size_t middleMark = (leftMark+rightMark)/2;
    Car *ans;
    
    while (leftMark<=rightMark){
        middleMark = (leftMark+rightMark)/2;
        ans = (cars)[middleMark];
        
        if (carID == ans->getID()) {
            return ans;
        }
        else if (carID > ans->getID()){
            leftMark = middleMark + 1;
        }
        else{
            rightMark = middleMark - 1;
        }
    }
    std::cout<< " Reached NULL findCar" <<std::endl;
    return NULL;
}


Simulator::~Simulator(){
    std::cout<< "Simulator Distructor Called" <<std::endl;
    for(vector<Car*>::iterator iter=cars.begin(); iter!=cars.end();++iter){
        delete (*iter);
        (*iter) = 0;
    }
    cars.clear();
    
    for(vector<Road*>::iterator iter=roads.begin(); iter!=roads.end();++iter){
        delete (*iter);
        (*iter) = 0;
    }
    roads.clear();
    
    for(vector<Junction*>::iterator iter=junctions.begin(); iter!=junctions.end();++iter){
        delete (*iter);
        (*iter) = 0;
    }
    junctions.clear();
    
    for(vector<Event*>::iterator iter=events.begin(); iter!=events.end();++iter){
        delete (*iter);
        (*iter) = 0;
    }
    events.clear();
    
    for(vector<Report*>::iterator iter=reports.begin(); iter!=reports.end();++iter){
        delete (*iter);
        (*iter) = 0;
    }
    reports.clear();
}
