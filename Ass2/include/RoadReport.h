//
//  RoadReport.h
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#ifndef __Assignment2__RoadReport__
#define __Assignment2__RoadReport__

#include <iostream>
#include "../include/Report.h"

class RoadReport : public Report {
private:
    Road *road;
    
public:
    RoadReport(string, int,  Road*);
    void writeReport();
    RoadReport(const RoadReport& other);
    void copy(const RoadReport& other);
    RoadReport& operator=(const RoadReport& other);
    string getType();
    virtual ~RoadReport();

};

#endif /* defined(__Assignment2__RoadReport__) */
