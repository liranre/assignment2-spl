//
//  Main.h
//  Ass2
//
//  Created by Liran Revivo on 11/30/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#ifndef __Ass2__Main__
#define __Ass2__Main__

#include <iostream>
#include <string>
#include <cmath>
#include <exception>
#include "../include/Car.h"
#include "../include/Road.h"
#include "../include/Junction.h"
#include "../include/CarReport.h"
#include "../include/RoadReport.h"
#include "../include/JunctionReport.h"
#include "../include/Report.h"
#include "../include/Event.h"
#include "../include/AddCarEvent.h"
#include "../include/CarFaultEvent.h"
#include "../include/Simulator.h"
using namespace std;





#endif /* defined(__Ass2__Main__) */
