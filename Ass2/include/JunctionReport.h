//
//  JunctionReport.h
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#ifndef __Assignment2__JunctionReport__
#define __Assignment2__JunctionReport__

#include "../include/Report.h"
#include <list>

class JunctionReport: public Report{
public:
    JunctionReport(string _reportID, int _time, Junction *_junction,list<Road*> _incomingRoads);
    void writeReport();
    JunctionReport(const JunctionReport& other);
    void copy(const JunctionReport& other);
    JunctionReport& operator=(const JunctionReport& other);
    string getType();
    virtual ~JunctionReport();

    
private:
    Junction *junction;
    list<Road*> comingRoads;
    
};

#endif /* defined(__Assignment2__JunctionReport__) */
