//
//  Simulator.h
//  Ass2
//
//  Created by Liran Revivo on 11/25/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#ifndef __Ass2__Simulator__
#define __Ass2__Simulator__

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include "../include/Car.h"
#include "../include/Road.h"
#include "../include/Junction.h"
#include "../include/Report.h"
#include "../include/CarReport.h"
#include "../include/RoadReport.h"
#include "../include/JunctionReport.h"
#include "../include/AddCarEvent.h"
#include "../include/CarFaultEvent.h"
#include "../include/Event.h"
using namespace std;
#define NO_TERMINATION -1

class Simulator{
private:
    // cars database
    vector<Car*> cars;
    // roads database
    vector<Road*> roads;
    // junction database
    vector <Junction*> junctions;
    // reports database
    vector <Report*> reports;
    // events database
    vector <Event*> events;
    
    int MAX_SPEED;
    int TERMINATION_TIME;
    int DEFAULT_TIME_SLICE;
    int MIN_TIME_SLICE;
    int MAX_TIME_SLICE;
    
    //functions
    Junction* findJunction(string junctionID);
    Road* findRoad (string startJunction, string endJunction);
    Car* findCar (string carID);
    void performSimulationForTime(int time);
    void performEvents(int time);
    void updateCarHistory(int time);
    void performCommands(int time);
    void advanceCars(int time);
    void updateCars(int time);
    void updateGreenLights(int time);
    void advanceCarsOnRoad(int time);
    void advanceCarsOnJunction(int time);
    
public:
    Simulator();
    // start the whole simulation of the system
    void beginSimulation();
    // read the commands from Commands.ini file
    void readCommands();
    // read events from Events.ini file
    void readEvents();
    // read configuration from Configuration.ini file
    void readConfiguration();
    // read road map from RoadMap.ini file
    void readRoadMap();
    Simulator(const Simulator& other);
    Simulator& operator=(const Simulator& other);
    int getTerminationTime();
    int getMaxSpeed();
    int getMaxTimeSlice();
    int getMinTimeSlice();
    int getDefaultTimeSlice();
    
    ~Simulator();
    
};


#endif /* defined(__Ass2__Simulator__) */
