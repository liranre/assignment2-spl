//
//  AddCarEvent.h
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#ifndef __Assignment2__AddCarEvent__
#define __Assignment2__AddCarEvent__

#include <iostream>
#include "../include/Event.h"
#include "../include/Car.h"
#include  <string>
#include <vector>
using namespace std;

class AddCarEvent: public Event{
public:
    AddCarEvent(): Event() ,simulatorCars(), type("car_arrival") , roadPlan(vector<Road*>()){}
    AddCarEvent(string _eventID, int _eventTime, string _carID, vector<Road*> _roadPlan, vector<Car*>* _simulationCars): Event(_eventID, _eventTime, _carID), simulatorCars(_simulationCars), type("car_arrival"), roadPlan(_roadPlan) {}
    string getRoadPlan();
    AddCarEvent(const AddCarEvent& other);
    void copy(const AddCarEvent& other);
    AddCarEvent& operator=(const AddCarEvent& other);
    void performEvent();
    virtual ~AddCarEvent();
    
private:
    vector<Car*>* simulatorCars;
    string type;
    vector<Road*> roadPlan;
};

#endif /* defined(__Assignment2__AddCarEvent__) */
