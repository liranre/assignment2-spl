//
//  Junction.h
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#ifndef __Assignment2__Junction__
#define __Assignment2__Junction__

#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/lexical_cast.hpp>
using namespace  std; 

class Junction{
public:
    Junction();
    Junction(string _ID);
    Junction(string _ID,queue<string> _passingRoads);
    Junction(const Junction& other);
    string getID() const;
    queue<string> getPassingRoads() const;
    int getNumOfPassingRoads() const;
    string getCurrentGreenRoad();
    void addRoadToIncomingRoads(string toAdd);
    // replace green roads
    void dequeuePassingRoads();
    string getCurrentGreenRoadStartJunction();
    string getCurrentGreenRoadEndJunction();
    Junction& operator=(const Junction& other);
    virtual ~Junction();
private:
    ///the junction ID
    std::string ID;
    //describes the roads which passing through the junction
    queue<string> passingRoads;
    
};

#endif /* defined(__Assignment2__Junction__) */
