//
//  Car.h
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#ifndef __Assignment2__Car__
#define __Assignment2__Car__

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "../include/Road.h"
#include "../include/Junction.h"
using namespace std;

class Road;

class Car{
public:
    Car();
    Car(string _ID,vector<Road*> _roadPlan,int _timeEntered,string _history);
    Car(string _ID,vector<Road*> _roadPlan,int _timeEntered,int _condition,int _faultTime , string _history, double _location,int _currentPlace);
    Car(const Car& other);
    string getID() const;
    vector<Road*> getRoadPlan() const;
    int getTimeArrival() const;
    int getCondition() const;
    int getFaultTime() const;
    string getHistory() const;
    double getLocation() const;
    int getPlace() const;
    Road* getCurrentRoad() const;
    void advanceInRoadPlan();
    void setLocation(double _newLocation);
    void setTimeArrival(int _newTimeEntered);
    void setCondition(int _newCondition);
    void setFaultTime(int _newFaultTime);
    void updateHistory(string newHistory);
    static bool compare(const Car* car1,const Car* car2);
    Car& operator=(const Car& other);
    virtual ~Car();
    

private:
    //car ID
    string ID;
    //describes the road plan of the specific car by sequence of junctions
    vector<Road*> roadPlan;
    //how much time is left for the car to be faulty
    // 0 - not faulty , x - faulty at time x
    int faultTime;
    //location on the road [0...length]
    double location;
    //describes the enter time to the simulation loop
    int timeEntered;
    string history;
    int currentPlace;
    int condition;
    

    
};

#endif /* defined(__Assignment2__Car__) */
