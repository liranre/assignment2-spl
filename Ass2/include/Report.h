//
//  Report.h
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#ifndef __Assignment2__Report__
#define __Assignment2__Report__

#include <iostream>
#include <string>
#include <vector>
#include "../include/Car.h"
#include "../include/Road.h"
#include "../include/Junction.h"
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/lexical_cast.hpp>
using namespace std;

class Report{
public:
    virtual void writeReport()=0;
    Report(string _reportID, int _time);
    int getTime();
    string getReportID();
    Report(const Report& other);
    void copy(const Report& other);
    Report& operator=(const Report& other);
    //override
    bool operator<(const Report& otherReport);
    virtual string getType() = 0;
    virtual ~Report();

    
protected:
    string reportID;
    int time;
};

#endif /* defined(__Assignment2__Report__) */
