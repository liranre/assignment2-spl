//
//  Event.h
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#ifndef __Assignment2__Event__
#define __Assignment2__Event__

#include <iostream>
#include <string>
#include "../include/Car.h"
#include "../include/Junction.h"
using namespace std;

class Event {
public:
    Event();
    Event(string _eventID, int _eventTime, string _carID);
    Event(const Event& other);
    void copy (const Event& other);
    virtual void performEvent()=0;
    int getTime();
    string getID();
    string getCarID();
    Event& operator=(const Event& other);
    virtual ~Event();
    
protected:
    string eventID;
    int eventTime;
    string carID;

};

#endif /* defined(__Assignment2__Event__) */
