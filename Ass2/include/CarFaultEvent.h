//
//  CarFaultEvent.h
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#ifndef __Assignment2__CarFaultEvent__
#define __Assignment2__CarFaultEvent__

#include <stdio.h>
#include "../include/Event.h"

class CarFaultEvent: public Event{
public:
    CarFaultEvent();
    CarFaultEvent(string _eventID, int _eventTime, string _carID, int _faultTime, Car* car): Event(_eventID,_eventTime,_carID), faultTime(_faultTime), car(car), type("car_fault"){}
    CarFaultEvent(const CarFaultEvent& other);
    void copy(const CarFaultEvent& other);
    CarFaultEvent& operator=(const CarFaultEvent& other);
    int getFaultTime();
    void performEvent();
    virtual ~CarFaultEvent();
    
private:
    int faultTime;
    Car* car;

    
protected:
     string type;
};

#endif /* defined(__Assignment2__CarFaultEvent__) */
