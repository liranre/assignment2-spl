//
//  CarReport.h
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#ifndef __Assignment2__CarReport__
#define __Assignment2__CarReport__

#include <stdio.h>
#include "../include/Report.h"

class CarReport: public Report{
public:
    CarReport(string _carReportID , int _time,string _carID , Car* _car);
    void writeReport();
    CarReport(const CarReport& other);
    void copy (const CarReport& other);
    CarReport& operator=(const CarReport& other);
    string getType();
    string getCarID();
    Car * getCar();
    virtual ~CarReport();

    
private:
    string carID;
    Car *car;
};

#endif /* defined(__Assignment2__CarReport__) */
