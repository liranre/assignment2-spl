//
//  Road.h
//  Assignment2
//
//  Created by Liran Revivo on 11/16/14.
//  Copyright (c) 2014 Liran Revivo. All rights reserved.
//

#ifndef __Assignment2__Road__
#define __Assignment2__Road__

#include <iostream>
#include "../include/Junction.h"
#include <vector>
#include <string>
#include "../include/Car.h"
#include <list>

class Car;
class Junction;

using namespace  std;

class Road{
public:
    Road();
    Road(double _length , Junction* _startJunction , Junction* _endJunction , int _timeSlice,int _hasGreentLight , int _numOfFaultyCars);
    Road(double _length , Junction* _startJunction , Junction* _endJunction , int _numOfFaultyCars, int _timeSlice , int _timeLeftFromTimeSlice , int _numOfPassingCars ,int _hasGreentLight , list<Car*> _carsOnRoad , list<Car*> _passingCars);
    Road(const Road& other);
    string getID() const;
    string getStartJunctionID() const;
    string getEndJunctionID() const;
    double getLength() const;
    Junction* getStartJunction() const;
    Junction* getEndJunction() const;
    int getNumOfCars() const;
    int getNumOfPassingCars() const;
    int getTimeSlice() const;
    int getTimeLeftFromTimeSlice() const;
    int getNumOfFaultyCars() const;
    list<Car*> getCarsOnRoad() const;
    list<Car*> getPassingCars() const;
    int getHasGreentLight() const;
    string printCarsOnRoad();
    bool isGreen() const;
    void setTimeLeftFromTimeSlice(int leftTimeSlice);
    void setNumPassingCars(int numOfCarsEntered);
    void setLight(int light);
    void setTimeSlice(int timeSlice);
    void addCarToRoad(Car* toAdd);
    void addCarToPassingRoad(Car* toAdd);
    void removeFromPassing(Car* toRemove);
    void removeFromRoad(Car* toRemove);
    void sortCarsOnRoad();
    Road& operator=(const Road& other);
    virtual~ Road();
    
    

private:
    string ID;
    double length;
    Junction* startJunction;
    Junction* endJunction;
    int numOfFaultyCars;
    int currentTimeSlice;
    int timeLeftFromTimeSlice;
    int numOfPassingCars;
    // if has green light = 1 , otherwise doesnt have green light
    int hasGreenLight;
    // cars currently on road
    list<Car*> carsOnRoad;
    // cars waiting to get on the road
    list<Car*> passingCars;
    string startJunctionID;
    string endJunctionID;

};

#endif /* defined(__Assignment2__Road__) */
